package com.bmapservice.bmapservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.bmapservice.controllers.SaveStudentInfoController;

@SpringBootApplication
@ComponentScan(basePackageClasses=SaveStudentInfoController.class)
public class BmapserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BmapserviceApplication.class, args);
	}
}
